package com.flockassems.main.constants;

public class DBConstants {

	public static final String ACCOUNTS = "accounts";
	
	public class Accounts{
		
		public static final String ACCOUNT_ID = "accountId";
		public static final String ACCOUNT_NAME = "name";
				
	}
	
}
