package com.flockassems.main.constants;

public class ResponseConstants {

	public static final String MESSAGE_IDS = "messageIds";
	public static final String VALID_PAYLOAD_COUNT = "validPayloadCount";
	public static final String INVALID_PAYLOADS = "invalidPayloads";
	public static final String ACCOUNTS_LIST = "accountsList";
}
