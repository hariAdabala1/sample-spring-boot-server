package com.flockassems.main.Controllers;

import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.flockassems.main.constants.DBConstants;
import com.flockassems.main.constants.ResponseConstants;
import com.flockassems.main.mappers.AccountRowMapper;
import com.flockassems.main.models.Accounts;

@RestController
public class demoConrollers {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private static Logger LOG = LogManager.getLogger(demoConrollers.class);
	
	private static final String SQL_ALL_ACCOUNTS = "select accountId, name from accounts";
	private static final String SQL_INSERT_ACCOUNTS = "insert into accounts (accountId,name) values (?,?)";
	private static final String SQL_UPDATE_ACCOUNTS = "update "+DBConstants.ACCOUNTS+" set "+DBConstants.Accounts.ACCOUNT_ID+" = ? where "+DBConstants.Accounts.ACCOUNT_NAME+" = ?";
	private static final String SQL_ALL_LIST_ACCOUNTS = "select "+DBConstants.Accounts.ACCOUNT_ID+", "+DBConstants.Accounts.ACCOUNT_NAME+" from "+DBConstants.ACCOUNTS+" where "+DBConstants.Accounts.ACCOUNT_ID+" = ? ";

	@RequestMapping(value = "api/v1/account/info/{accountID}", method = {
			RequestMethod.GET }, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity queryAccount(@RequestHeader("Authorization") String authorization,
			@PathVariable String accountID) throws SQLException, JSONException {
		if (accountID != null) {
			LOG.info(HttpStatus.ACCEPTED);
			return new ResponseEntity<>(new JSONObject().put(ResponseConstants.MESSAGE_IDS, accountID).toString(),
					HttpStatus.ACCEPTED);
		}
		return null;
	}

	@RequestMapping(value = "api/v1/account/info/{accountID}", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity testAccount(@RequestHeader("Authorization") String authorization,
			@RequestBody String json, @PathVariable String accountID) throws SQLException, JSONException {
		if (accountID != null) {

			JSONObject js = new JSONObject(json);
			String message = js.getString("account");

			JSONArray accountsJson = new JSONArray(jdbcTemplate.queryForList(SQL_ALL_ACCOUNTS));

			JSONObject jsonObject = new JSONObject();

			jsonObject.put(ResponseConstants.MESSAGE_IDS, message);
			jsonObject.put(ResponseConstants.ACCOUNTS_LIST, accountsJson);

			LOG.info(HttpStatus.ACCEPTED);
//			return new ResponseEntity<>(new JSONObject().put(ResponseConstants.MESSAGE_IDS, message).toString(),HttpStatus.ACCEPTED);
			return new ResponseEntity<>(jsonObject.toString(), HttpStatus.ACCEPTED);
		}

		return null;
	}

	@RequestMapping(value = "api/v1/account/infoClass/{accountID}", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity testAccountClass(@RequestHeader("Authorization") String authorization,
			@RequestBody Accounts accountClass, @PathVariable String accountID) throws SQLException, JSONException {
		if (accountID != null) {

			String message = accountClass.getAccountid();

	        // define query arguments
	        Object[] params = new Object[] { accountClass.getAccountid(), accountClass.getName()};
	         
	        // define SQL types of the arguments
	        int[] types = new int[] { Types.VARCHAR, Types.VARCHAR};
	 
			jdbcTemplate.update(SQL_INSERT_ACCOUNTS, params, types);
			JSONArray accountsJson = new JSONArray(jdbcTemplate.queryForList(SQL_ALL_ACCOUNTS));

			JSONObject jsonObject = new JSONObject();

			jsonObject.put(ResponseConstants.MESSAGE_IDS, message);
			jsonObject.put(ResponseConstants.ACCOUNTS_LIST, accountsJson);

			LOG.info(HttpStatus.ACCEPTED);
//			return new ResponseEntity<>(new JSONObject().put(ResponseConstants.MESSAGE_IDS, message).toString(),HttpStatus.ACCEPTED);
			return new ResponseEntity<>(jsonObject.toString(), HttpStatus.ACCEPTED);
		
		}
		return null;
	}
	
	@RequestMapping(value = "api/v1/account/infoUpdateWithRowMapper/{accountID}", method = {
			RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity updateAccountClass(@RequestHeader("Authorization") String authorization,
			@RequestBody Accounts accountClass, @PathVariable String accountID) throws SQLException, JSONException {
		if (accountID != null) {

			String message = accountClass.getAccountid();

	 
			jdbcTemplate.update(SQL_UPDATE_ACCOUNTS, accountClass.getAccountid(), accountClass.getName());

//			List<Accounts> accountList = jdbcTemplate.query(SQL_ALL_LIST_ACCOUNTS,
//			new RowMapper());
			List<Accounts> accountList = null;
			accountList = jdbcTemplate.query(
					SQL_ALL_LIST_ACCOUNTS, 
					new String[] { accountClass.getAccountid() }, 
					new AccountRowMapper());
			
//			JSONArray accountsJson = new JSONArray(jdbcTemplate.queryForList(SQL_ALL_ACCOUNTS));

			JSONObject jsonObject = new JSONObject();

			jsonObject.put(ResponseConstants.MESSAGE_IDS, message);
			jsonObject.put(ResponseConstants.ACCOUNTS_LIST, accountList);

			LOG.info(HttpStatus.ACCEPTED);
//			return new ResponseEntity<>(new JSONObject().put(ResponseConstants.MESSAGE_IDS, message).toString(),HttpStatus.ACCEPTED);
			return new ResponseEntity<>(jsonObject.toString(), HttpStatus.ACCEPTED);
		
		}
		return null;
	}

}
