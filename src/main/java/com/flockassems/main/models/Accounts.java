package com.flockassems.main.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.flockassems.main.constants.JsonConstants;

//@JsonIgnoreProperties(ignoreUnknown = true)
public class Accounts {
	
	@JsonProperty(value = JsonConstants.ACCOUNT_ID)
	private String accountid;

	@JsonProperty(value = JsonConstants.NAME)
	private String name;

	public String getAccountid() {
		return accountid;
	}

	public void setAccountid(String accountid) {
		this.accountid = accountid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
