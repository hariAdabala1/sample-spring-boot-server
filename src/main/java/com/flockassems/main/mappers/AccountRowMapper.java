package com.flockassems.main.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;

import com.flockassems.main.constants.DBConstants;
import com.flockassems.main.models.Accounts;


public class AccountRowMapper extends BeanPropertyRowMapper<Accounts>{
	
	public Accounts mapRow(ResultSet rs, int rowNumber) throws SQLException {
		Accounts accounts = new Accounts();
		accounts.setAccountid(rs.getString(DBConstants.Accounts.ACCOUNT_ID));
		accounts.setName(rs.getString(DBConstants.Accounts.ACCOUNT_NAME));

		
//		if (strGroups != null && !strGroups.isEmpty()) {
//			List<String> groups = Arrays.asList(strGroups.split(SymbolConstants.COMMA));
//		}
//		campaign.setGroupIds(new ArrayList<>());
		
		
		return accounts;
	}
}
